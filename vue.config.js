const path = require("path");
const PrerenderSPAPlugin = require("prerender-spa-plugin");

module.exports = {
  // options...
  //publicPath: '',
  devServer: {
    https: true
  },
  css: {
    loaderOptions: {
      sass: {
        data: `@import "@/assets/scss/main.scss";`
      }
    }
  },
  configureWebpack:  {
    plugins: process.env.NODE_ENV === 'production' ? [
      new PrerenderSPAPlugin({
        // Required - The path to the webpack-outputted app to prerender.
        staticDir: path.join(__dirname, 'dist'),
        // Required - Routes to render.
        routes: [ '/', '/about', '/services', '/projects', '/contact', '/privacypolicy'],
      })
    ] : []
  },
    pluginOptions: {
      sitemap: {
        urls: [
          'https://rbd.com.au/',
          'https://rbd.com.au/about',
          'https://rbd.com.au/projects',
          'https://rbd.com.au/services',
          'https://rbd.com.au/contact',
      ]
      }
  }
}
